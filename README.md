## Welcome to VPS Script

you can using this script to install to you vps and manager all website in vps

### Install

Just run it in you vps

Centos 7
```curl
curl -sO https://script.tranvanphi.info/install && bash install
```
Ubuntu (nginx - mariadb - fail2ban)
```curl
curl -sO https://script.tranvanphi.info/i-ubuntu-001 && bash i-ubuntu-001
```

### Script using
* PHPMyadmin: 5.0.3
* Extplorer: 2.1.13
* PHP: 7.4
* Nginx: 1.18.0-2.el7
* Mariadb: 10.3
